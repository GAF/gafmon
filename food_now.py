#!/usr/bin/python2

import random
import bisect
import datetime
import os, sys

def getTex():
	random.seed(str(datetime.date.today()))
	foods = []
	# Aufbau der foodlist: name, restaurant-typ (0=normal, 1=takeaway, >2=todo), offene Tage, Tagesgewichte
	with open(os.path.join(os.path.dirname(sys.argv[0]), 'foodlist.enterprise.xml')) as f:
		foods = f.read().split('\n')

	dinners = []
	for line in foods:
		splitline = line.split(',')
		if len(splitline) <4:
			continue;
		actual_line = {"name":splitline[0], "type":int(splitline[1]), "open_days":splitline[2], "daily_preference":splitline[3:len(splitline)] }
		dinners.append(actual_line)
	current_day = datetime.date.weekday(datetime.date.today()) + 1
	open_dinners = []
	for i in dinners:
		restaurant_open = str(current_day) in i["open_days"]
		if restaurant_open:
			current_preference = int(i["daily_preference"][current_day - 1])
			restaurant = ({"name":i["name"],"type":i["type"]},current_preference)
			open_dinners.append(restaurant)
	choices = []
	takeaway = False
	for i in range(3):
		choice = weighted_choice(open_dinners)
		if choice[0]["type"] == 1:
			takeaway == True
		elif i == 2:
			# using for to prevent a potential infinite loop
			for _ in choices:
				choice = weighted_choice(open_dinners)
				if choice[0]["type"] == 1:
					takeaway = True
				if takeaway == True:
					break;
		choices.append(choice[0]["name"])
		open_dinners.remove(choice)
	return "Restaurants des Tages: "+", ".join(choices)

def weighted_choice(choices):
	diners, weight = zip(*choices)
	total = 0
	cumulative_weights = []
	for w in weight:
		total += w
		cumulative_weights.append(total)
	x = random.random() * total
	i = bisect.bisect(cumulative_weights, x)
	return choices[i]

if __name__=='__main__':
    print(getTex())
