#!/usr/bin/python2
# -*- coding: UTF-8 -*-
import io, os, re, sys, time, locale, datetime
import mvglivetex, openmensatex, whentex
import food_now

locale.setlocale(locale.LC_ALL, 'de_DE.UTF-8')

def smartquote(s):
	return re.sub("\"(.*)\"", "\"`\\1\"'", s)

stations = {
	'Pinakotheken':         ['27', '28', 'N27', 'N28', '37', '38', '100', '150'],
	'Schellingstraße':      ['154'],
	'Universität':          ['U3', 'U6'],
	'Theresienstraße':      ['U2', 'U8'],
	'Odeonsplatz':          ['U5'],
	'Karlsplatz (Stachus)': ['N19', 'N45'],
        'Von-der-Tann-Straße':  ['N45'],
        'Nationaltheater':      ['N19'],
}

BASEDIR = os.path.dirname(os.path.realpath(__file__))

today = datetime.datetime.today()
if today.month == 3 and today.day == 17:
	textcolor = "green"
else:
	textcolor = "white"

tex = []
tex.append(r"""
\documentclass[ngerman,twocolumn]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[paperwidth=12.5cm,paperheight=10cm,margin=.2cm,top=.5cm]{geometry}
\usepackage{graphicx}
\usepackage{lmodern}
\usepackage{multirow}
\usepackage{rotating}
\usepackage{booktabs}
\usepackage{polyglossia}
\usepackage{textcomp}
\usepackage{tabularx}
\usepackage{supertabular}
\usepackage{pifont}

\usepackage{xcolor}
\pagecolor{black}
\pagestyle{empty}

\newcolumntype{R}{>{\raggedleft\arraybackslash}X}%%
\newcolumntype{L}{>{\raggedright\arraybackslash}X}%%
\newcolumntype{C}{>{\centering\arraybackslash}X}%%

\setlength{\columnsep}{.5cm}
\setlength{\parindent}{0cm} 

\begin{document}
\color{%s}
\sffamily
\flushbottom
 
""" % textcolor)

try:
    tex.append(mvglivetex.getTable(stations))
except:
    tex.append("TODO: FIX " + mvglivetex.__file__)

tex.append(r"""
\rmfamily
\begin{tabularx}{\linewidth}{@{}lR@{}}
\raisebox{.2em}{\large %s} & \multirow{2}{*}{\fontsize{1.2cm}{1em}\selectfont %s} \\
\raisebox{-.2em}{\large %s} \\
\end{tabularx}
\sffamily
\vspace{.3cm}
""" % (time.strftime('%A'), time.strftime('%H:%M'), time.strftime('%x')))

# tex.append(r"\fontsize{0.8cm}{1em}"+food_now.getTex())
try:
    tex.append(food_now.getTex())
except:
    tex.append("TODO: FIX " + food_now.__file__)

tex.append("")
tex.append(r"\vspace{0.5em}")

try:
    tex.append(smartquote(openmensatex.getTable()))
except:
    tex.append("TODO: FIX " + openmensatex.__file__)

tex.append("")
tex.append(r"\vspace{1em}")

try:
    tex.append(whentex.getTex())
except:
    tex.append("TODO: FIX " + whentex.__file__)

tex.append(r"""
\end{document}
""")

with io.open(BASEDIR + '/mvg.tex', 'w', encoding='utf-8') as f:
	f.write('\n'.join(tex))

os.system("xelatex mvg.tex")
