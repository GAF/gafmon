#!/usr/bin/python

import locale, os, os.path, pygments.lexers, re, subprocess, sys, time, urllib
#locale.setlocale(locale.LC_ALL, 'de_DE.UTF-8')

URL = 'http://www.studentenwerk-muenchen.de/mensa/speiseplan/speiseplan_%Y-%m-%d_421_-de.html' 

if len(sys.argv) == 2:
	wrap = int(sys.argv[1])
else:
	wrap = 40

def fetch(t, tmpdir='/var/tmp'):
	fn = os.path.join(tmpdir, time.strftime('mensa-%Y-%m-%d', t))
	#urllib.urlretrieve(time.strftime(URL, t), fn)[1]
	return open(fn, 'r').read()

def parse(data):
	bucket = []
	active = -1
	lexer = pygments.lexers.get_lexer_for_mimetype('text/html', encoding='utf-8')
	data = data.replace('&amp;', "'n'") #XXX
	for t, v in lexer.get_tokens(data):
		if (str(t) == 'Token.Name.Tag' and v == '<table'):
		        active += 1
		if (str(t) == 'Token.Literal.String' and v == '"menu"'):
		        active += 1
		if (str(t) == 'Token.Name.Tag' and v == '</table>'):
		        active = -1
		if (active <= 0):
		        continue
		if (str(t) != 'Token.Text' or not v.strip() or v.count('\t\t\t') or v in ('fleischlos', 'mit Fleisch', 'PDF', 'vegan')):
			continue
		bucket.append(v)
	return bucket

def format(bucket, wrap=wrap):
	c, text = 0, ''
	for token in bucket:
		if re.match('(?:[TAB]\d+|Beilagen|Aktion)', token):
			text += '\n' + token + ' '
			c = 0
		elif c > 0 and c + len(token) >= wrap:
			text += ',\n' + token
			c = len(token)
		else:
			if c > 0:
				text += ', ' + token
			else:
				text += token
			c += len(token) + 2
	return text

def format2(bucket, wrap=wrap):
	c, text = 0, ''
	for token in ', '.join(bucket).split():
		if re.match('(?:[TAB]\d+|Beilagen|Aktion)', token) or (c > 1 and c + 1 + len(token) > wrap):
			token = token.rstrip(',')
			c, text = 0, text.rstrip(', ') + '\n'
		#if c:
		c, text = c + 1, text + ' '
		c, text = c + len(token), text + token
	return text

shorten = lambda s: re.sub(r'([TAB])(?:agesgericht|ktionsessen|iogericht) (\d+)', r'\1\2', s)

highlight = lambda s: re.sub(r'([TAB]\d+|Beilagen|Aktion)', r'${color1}\1${color}', s)

t = time.localtime()
if t.tm_hour * 60 + t.tm_min > 15 * 60 + 50:
    advance = 1
else:
    advance = 0

while True:
	mt = time.localtime(time.time() + advance * 86400)
	output = highlight(format(map(shorten, parse(fetch(mt)))))
	if output or advance > 30:
		break
	advance += 1

advance = {0: 'heute', 1: 'morgen'}.get(advance, 'in %d Tagen' % advance)
print ('Mensa Arcisstr %s am %s' % (advance, output))
