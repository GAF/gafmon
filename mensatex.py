#!/usr/bin/python2
# -*- coding: UTF-8 -*-

import locale, os, os.path, pygments.lexers, re, subprocess, sys, time, urllib
#locale.setlocale(locale.LC_ALL, 'de_DE.UTF-8')

URL = 'http://www.studentenwerk-muenchen.de/mensa/speiseplan/speiseplan_%Y-%m-%d_421_-de.html' 

if len(sys.argv) == 2:
        wrap = int(sys.argv[1])
else:
        wrap = 40

def fetch(t, tmpdir='/var/tmp'):
        fn = os.path.join(tmpdir, time.strftime('mensa-%Y-%m-%d', t))
        #urllib.urlretrieve(time.strftime(URL, t), fn)[1]
        if not os.path.exists(fn):
            print('Mensa data %s not found' % fn)
            return ''
        else:
            return open(fn, 'r').read()

def parse(data):
        bucket = []
        active = -1
        lexer = pygments.lexers.get_lexer_for_mimetype('text/html', encoding='utf-8')
        data = data.replace('&amp;', "'n'") #XXX
        for t, v in lexer.get_tokens(data):
                if (str(t) == 'Token.Name.Tag' and v == '<table'):
                        active += 1
                if (str(t) == 'Token.Literal.String' and v == '"menu"'):
                        active += 1
                if (str(t) == 'Token.Name.Tag' and v == '</table>'):
                        active = -1
                if (active <= 0):
                        continue
                if (str(t) != 'Token.Text' or not v.strip() or v.count('\t\t\t') or v in ('fleischlos', 'mit Fleisch', 'PDF', 'vegan')):
                        continue
                bucket.append(v)
        return bucket

def format(bucket, wrap=wrap):
    if not bucket: return

    bucket = map(lambda s: re.sub(r"\([^fv].*?\)", "", s), bucket)
    day = bucket[0]
    food = []
    beilagen = []
    aktion = []
    i = 1
    while i < len(bucket):
        if re.match('(?:[TABS](\d+|S))', bucket[i]):
            if not bucket[i+1] == 'Salatbuffet':
                food.append((bucket[i], bucket[i+1]))
            i += 2
        elif bucket[i] == "Beilagen":
            i += 1
            break
        else:
            print("WHAT IS " + bucket[i])
            i += 1
    while i < len(bucket):
        if bucket[i] == "Aktion":
            aktion = map(lambda s: s.strip(), bucket[i+1:])
            break
        else:
            beilagen.append(bucket[i].strip())
            i += 1

    tex = []
    tex.append(r"""

    \begingroup
    \fontsize{8pt}{9pt}\selectfont

    \begin{tabularx}{\linewidth}{@{}l@{\ }L@{}}
    \midrule
    """)

    for (v,f) in food:
        tex.append(r" %s & %s \\"%(v,re.sub(r"\bund\b", r"\\&", f)))

    tex.append(r"""
    \midrule
    \end{tabularx}

    \fontsize{6pt}{7pt}\selectfont
    \textbf{Beilagen:} %s\\
    \textbf{Aktion:} %s

    \endgroup
    """%(', '.join(map(lambda s: s.replace (', natur', ' \\textit{natur}'), beilagen)), ', '.join(aktion)))

    return u'\n'.join(tex)

shorten = lambda s: re.sub(r' \[.*\]', '', re.sub(r'([TAB])(?:agesgericht|ktionsessen|iogericht) (\d+)', r'\1\2', s).replace('Self-Service', 'SS'))

def getTable():
    tex = []

    t = time.localtime()
    if t.tm_hour * 60 + t.tm_min > 15 * 60 + 50:
        advance = 1
    else:
        advance = 0

    output = None
    while True:
        mt = time.localtime(time.time() + advance * 86400)
        output = format(map(shorten, parse(fetch(mt))))
        if output or advance >= 28:
            break
        advance += 1
   
    if output:
        advance = {0: 'heute', 1: 'morgen'}.get(advance, 'in %d Tagen' % advance)
        tex.append(u"Mensa Arcisstraße %s\\" % advance)
        tex.append(output)
    else:
        tex.append('Download new mensa data via /usr/local/bin/get\_mensa.py or fix parser.')
    return '\n'.join(tex)

if __name__ == '__main__':
    print(getTable())
