#!/usr/bin/python2
# -*- coding: UTF-8 -*-
from collections import defaultdict
import MVGLive, re

## /!\ Keine Namensänderungen für Stationen, außer der maximalen Endhaltestelle.
shortdest = {
	#### Bus
	(u'100', u'Bus',    u'Hauptbahnhof Nord'):          (u'100', u'Hauptbahnho Fnord', u''),
	
	(u'100', u'Bus',    u'Ostbahnhof'):                 (u'100', u'Prostbahnhof', u''),
	
	
	(u'150', u'Bus',    u'Tivolistraße'):               (u'150',u'Raviolistraße', u''),
	
	(u'150', u'Bus',    u'Hauptbahnhof Nord'):          (u'100',u'Hauptbahnho Fnord', u''),
	
	
	## Nordbad - (Uni) - Arabellapark - Bruno-Walter-Ring
	(u'154', u'Bus',    u'Bruno-Walter-Ring'):          (u'154',u'Brüno-Schalter-Ding', u''),
	(u'154', u'Bus',    u'Arabellapark'):               (u'154',u'Brüno-Schalter-Ding', u'a'),
	
	(u'154', u'Bus',    u'Nordbad'):                    (u'154',u'Not bad', u''),
	
	
	#### U-Bahn
	(u'U2',  u'U-Bahn', u'Harthof'):                    (u'U2', u'Feldmosching', u'H'),
	(u'U2',  u'U-Bahn', u'Milbertshofen'):              (u'U2', u'Feldmosching', u'M'),
	(u'U2',  u'U-Bahn', u'Feldmoching'):                (u'U2', u'Feldmosching', u'M'),
	
	(u'U2',  u'U-Bahn', u'Messestadt Ost'):             (u'U2', u'Messerschmitt Ost', u''),
	(u'U2',  u'U-Bahn', u'Innsbrucker Ring'):           (u'U2', u'Messerschmitt Ost', u'I'),
	(u'U2',  u'U-Bahn', u'Kolumbusplatz'):              (u'U2', u'Messerschmitt Ost', u'K'),
	(u'U2',  u'U-Bahn', u'Sendlinger Tor'):             (u'U2', u'Messerschmitt Ost', u'S'),
	
	
	(u'U3',  u'U-Bahn', u'Fürstenried West'):           (u'U3', u'Fürstenmeet West', u''),
	(u'U3',  u'U-Bahn', u'Sendlinger Tor'):             (u'U3', u'Fürstenmeet West', u'S'),
	(u'U3',  u'U-Bahn', u'Thalkirchen'):                (u'U3', u'Fürstenmeet West', u'T'),
	
	(u'U3',  u'U-Bahn', u'Moosach'):                    (u'U3', u'Mo sach', u''),
	(u'U3',  u'U-Bahn', u'Olympiazentrum'):             (u'U3', u'Mo sach', u'O'),
	(u'U3',  u'U-Bahn', u'Münchner Freiheit'):          (u'U3', u'Mo sach', u'F'),
	
	(u'U5',  u'U-Bahn', u'Laimer Platz'):               (u'U5', u'Lahmer Platz', u''),
	(u'U5',  u'U-Bahn', u'Theresienwiese'):             (u'U5', u'Lahmer Platz', u'W'),
	(u'U5',  u'U-Bahn', u'Westendstraße'):              (u'U5', u'Lahmer Platz', u'E'),
	
	(u'U5',  u'U-Bahn', u'Neuperlach Süd'):             (u'U5', u'Brauperlach Sud', u''),
	(u'U5',  u'U-Bahn', u'Neuperlach Zentrum'):         (u'U5', u'Brauperlach Sud', u'Z'),
	
	
	(u'U6',  u'U-Bahn', u'Garching-Forschungszentrum'): (u'U6', u'Novogarchinsk', u''),
	(u'U6',  u'U-Bahn', u'Alte Heide'):                 (u'U6', u'Novogarchinsk', u'A'),
	(u'U6',  u'U-Bahn', u'Kieferngarten'):              (u'U6', u'Novogarchinsk', u'K'),
	(u'U6',  u'U-Bahn', u'Ditlindenstraße'):            (u'U6', u'Novogarchinsk', u'D'),
	(u'U6',  u'U-Bahn', u'Münchner Freiheit'):          (u'U6', u'Novogarchinsk', u'F'),
	(u'U6',  u'U-Bahn', u'Fröttmaning'):                (u'U6', u'Novogarchinsk', u'D'),
	
	(u'U6',  u'U-Bahn', u'Klinikum Großhadern'):        (u'U6', u'Groß-Hadernograd', u''),
	(u'U6',  u'U-Bahn', u'Harras'):                     (u'U6', u'Groß-Hadernograd', u'h'),
	(u'U6',  u'U-Bahn', u'Implerstraße'):               (u'U6', u'Groß-Hadernograd', u'I'),
	
	
	(u'U8',  u'U-Bahn', u'Kolumbusplatz'):              (u'U2', u'Messerschmitt Ost', u'I'),
	(u'U8',  u'U-Bahn', u'Moosach'):                    (u'U3', u'Mo sach', u'I'),
	
	
	#### Tram
	(u'27',  u'Tram',   u'Karlsplatz Nord'):            (u'27', u'Karlsplatz Fnord', u''),
	(u'28',  u'Tram',   u'Karlsplatz Nord'):            (u'27', u'Karlsplatz Fnord', u''),  ## ???
	(u'27',  u'Tram',   u'Petuelring'):                 (u'27', u'P-ethylring', u''),
	(u'28',  u'Tram',   u'Scheidplatz'):                (u'28', u'Gscheidplatz', u''),
	(u'28',  u'Tram',   u'Einsteinstraße'):             (u'28', u'1st1straße', u''),
	(u'27',  u'Tram',   u'Einsteinstraße'):             (u'28', u'1st1straße', u''),  ## ???
	(u'N27', u'Tram',   u'Karlsplatz Nord'):            (u'N27', u'Karlsplatz Fnord', u''),
	(u'N28', u'Tram',   u'Karlsplatz Nord'):            (u'N27', u'Karlsplatz Fnord', u''),  ## ???
	(u'N27', u'Tram',   u'Petuelring'):                 (u'N27', u'P-ethylring', u''),
	(u'N28', u'Tram',   u'Scheidplatz'):                (u'N28', u'Gscheidplatz', u''),
	(u'N28', u'Tram',   u'Einsteinstraße'):             (u'N28', u'1st1straße', u''),
	(u'N27', u'Tram',   u'Einsteinstraße'):             (u'N28', u'1st1straße', u''), ## ???
	
	(u'N27', u'Nachteule', u'Großhesseloher Brücke'):   (u'N27', u'Großhess. Brücke', u''), ## ???
	
	(u'U3',  u'SEV',    u'SEV Karlsplatz (Stachus)'):   (u'U3', u'SEV Karlsplatz', u''), ## ???
}


def toInt(text):
	return int(re.sub('\D', '', text) or -1)


def renderStation(station, departures):
	if not departures:
		return None
	
	tex = []
	
	#tex.append('%% STATION ' + station)
	
	for (line,prod,dest),v in sorted(departures.iteritems(), key=lambda ((s,_,dest),x) : (toInt(s),dest)):
		tex.append(renderDeparture(line, prod, dest, v))
	
	return '\n'.join(tex)


def renderDeparture(line, prod, dest, v):
	tex = []
	
	## Product icon
	tex.append(ur"""\includegraphics[height=.8em]{%s.pdf} & """ % prod)
	
	## Line icon/name
	if 0 < toInt(line) <= 8:
		tex.append(ur"\includegraphics[height=.8em]{%s.pdf} & " % line)
	else:
		tex.append("%s & " % line)
	
	## Next 3 departures
	tex.append(ur"%s & %s \\" % (
			dest,
			r",\,".join(map(lambda (t,sup) : 
				r"\hphantom{55}\llap{%i}" % t + (sup and
					r"\rlap{\textsuperscript{\tiny{%s}}}" % sup),
				sorted(v)[:3]))
			))
	
	return '\n'.join(tex)


def getTable(stations):
	alldepartures = {}
	for (station,lines) in stations.iteritems():
		departures = defaultdict(list)
		data = MVGLive.getlivedata(station, 50)
		for departure in data:
			line = departure['linename'].decode('utf-8')
			if not line in lines:
				continue
			k = ( line, departure['product'], departure['destination'])
			l = departures.get(k, [])
			l.append((int(departure['time']), r''))
			departures[k] = l
		alldepartures[station] = departures
	
	if (u'28', u'Tram', u'Sendlinger Tor') in alldepartures['Pinakotheken']:
		shortdest[u'27', u'Tram', u'Sendlinger Tor'] = (ur'27', u'Send ping, Tor!', u'')
		shortdest[u'28', u'Tram', u'Sendlinger Tor'] = (ur'27', u'Send ping, Tor!', u'')
	
	## rename and collapse some destinations
	usedshorts = {}
	for departures in alldepartures.itervalues():
		for ((oldline,prod,olddest),(newline,newdest,short)) in shortdest.iteritems():
			i = departures.get((oldline,prod,olddest), [])
			if i:
				departures[newline,prod,newdest] += map(lambda(t,_):(t,short), i)
				del departures[oldline,prod,olddest]
				if short:
					usedshorts[short] = olddest
	
	tex = []
	tex.append(ur"""
	\begin{tabularx}{\linewidth}{@{}l@{\,}c@{\ }L@{\,}r@{}}
	\rlap{Linie} & \hphantom{555} & Richtung & \hphantom{55,\,55,\,55}\llap{Abfahrten} \\
	\toprule
	""")
	
	renderedStations = map(lambda (s,d): renderStation(s, d), alldepartures.iteritems())
	renderedStations = filter(lambda x: x is not None, renderedStations)
	tex.append('\n\n\\midrule\n\n'.join(renderedStations))
	
	tex.append(ur"\bottomrule\end{tabularx}")
	
	if usedshorts:
		tex.append(ur"\begin{tabularx}{\linewidth}{@{}L@{}}\tiny Abkürzungen: ")
		tex.append(' / '.join(map(lambda x: ur'%s:~%s ' % x, usedshorts.iteritems())))
		tex.append(ur'\end{tabularx}')
	
	return u'\n'.join(tex)
