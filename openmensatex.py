#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import json, time, os, sys, re

if len(sys.argv) == 2:
        wrap = int(sys.argv[1])
else:
        wrap = 40

def fetch(t, tmpdir='/var/tmp'):
    fn = os.path.join(tmpdir, time.strftime('mensa-%Y-%m-%d', t))
    if not os.path.exists(fn):
        print('Mensa data %s not found' % fn)
        return ''
    elif (os.path.getsize(fn) == 0):
        # empty file, most likely because the mensa is not open
        return False
    else:
        jsonstuff = []
        with open(fn, "r") as json_data:
            jsonstuff = json.load(json_data)
        return jsonstuff

def parse(data):
    if not data:
        return False
    result = []
    for i in data:
        result.append((i['name'], i['category']))
    return result

def transFood(name):
    if name == "Indisches Linsencurry":
        return "Freddy's indisches Linsen-mehr-curry"
    
    name = name \
    .replace(u"Hirtenkäse", u"Hirnkäse") \
    .replace("scharf", "schaf") \
    .replace("tasche", "handtasche") \
    .replace("gulasch", "gularsch") \
    .replace("Koriander", "Korianer") \
    .replace("Soja", "Sonja") \
    .replace("Rababer", "Rababera") \
    .replace("Pesto", "Pest") \
    .replace("&", "\\&")
    
    return name

def format(bucket, wrap=wrap):
    if not bucket: return

    food = []
    beilagen = []
    aktion = []
    tex = []
    selfservice = []
    for (name, art) in bucket:
        name = transFood(name)
        if art == "Beilagen":
            beilagen.append(name)
        elif re.match("Aktion", art):
            aktion.append(name)
        elif re.match("Tagesgericht", art):
            food.append((re.sub(r"agesgericht ", "", art), name))
        elif re.match("Self-Service$", art):
            selfservice.append(name)
        elif re.match("Self-Service ", art):
            selfservice.append("{\color{lime}\ding{168}}"+name)
        elif art == "Baustellenteller":
            food.append(("BS", name))
        elif art == "Fast Lane":
            food.append(("FTL", name))
        else:
            print("sb plz play regex golf with " + art)
            food.append((art, name))

    if (len(selfservice) != 0):
        food.append(("SS", ', '.join(selfservice)))

    tex.append(r"""

    \begingroup
    \fontsize{8pt}{9pt}\selectfont

    \begin{tabularx}{\linewidth}{@{}l@{\ }L@{}}
    \midrule
    """)

    for (v,f) in food:
        tex.append(r" %s & %s \\"%(v,re.sub(r"\bund\b", r"\\&", f)))

    tex.append(r"""
    \midrule
    \end{tabularx}

    \fontsize{6pt}{7pt}\selectfont
    \textbf{Beilagen:} %s\\
    \textbf{Aktion:} %s

    \endgroup
    """%(', '.join(map(lambda s: re.sub(r"&", "\\&", s.replace (', natur', ' \\textit{natur}')), beilagen)), ', '.join(aktion)))

    return u'\n'.join(tex)

def getTable():
    tex = []

    t = time.localtime()
    if t.tm_hour * 60 + t.tm_min > 15 * 60 + 50:
        advance = 1
    else:
        advance = 0

    output = None
    while True:
        mt = time.localtime(time.time() + advance * 86400)
        output = format(parse(fetch(mt)))
        if output or advance >= 28:
            break
        advance += 1

    if output:
        advance = {0: 'heute', 1: 'morgen'}.get(advance, 'in %d Tagen' % advance)
        tex.append(u"Mensa Arcisstraße %s\\" % advance)
        tex.append(output)
    else:
        tex.append('Download new mensa data via /usr/local/bin/get\_mensa.py or fix parser.')
    return '\n'.join(tex)

if __name__ == '__main__':
    print(getTable())

# vim: set expandtab ts=4:
