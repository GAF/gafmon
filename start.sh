#!/bin/sh

pushd /home/gaf/gafmon
./gafmon.py
zathura mvg.pdf &
echo $! > ~/.gafmon.pid

xdotool search --classname zathura windowmove 0 0 key f key W

while true; do
	sleep 30
	./gafmon.py
done

popd
